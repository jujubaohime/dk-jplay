/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import jplay.Animation;
import jplay.Keyboard;
import jplay.Window;
import jplay.GameImage;
import java.awt.*;


public class Program {
    public static void main(String[] args){
        Window janela = new Window(744,736);
        GameImage background = new GameImage(".\\sprites\\stage1.png");


        Animation dkParado = new Animation(".\\sprites\\dk.png", 8);
        Animation deisy = new Animation(".\\sprites\\deisy.png", 20);
        Animation peachRight = new Animation(".\\sprites\\peachRight.png", 3);
        Animation peachLeft = new Animation(".\\sprites\\peachLeft.png", 3);
        Animation peachHammerRight = new Animation(".\\sprites\\peachHammerRight.png", 4);
        Animation peachHammerLeft = new Animation(".\\sprites\\peachHammerLeft.png", 4);
        Animation peachLadder = new Animation(".\\sprites\\peachLadder.png", 3);
        Animation barrel = new Animation(".\\sprites\\barrel.png", 8);
        Animation dkThrow = new Animation(".\\sprites\\dkthrow.png", 6);

        dkParado.x = 13;dkParado.y = 53;
        barrel.x = 85; barrel.y = 189;
        dkThrow.x = 277; dkThrow.y = 10;
        peachLadder.x = 588; peachLadder.y = 226;
        peachHammerRight.x = 22; peachHammerRight.y = 338;
        peachHammerLeft.x = 285; peachHammerLeft.y = 249;
        peachLeft.x = 512; peachLeft.y = 580;
        peachRight.x = 407; peachRight.y = 670;
        deisy.x = 170; deisy.y = 53;

        dkParado.setTotalDuration(1200);
        barrel.setTotalDuration(1600);
        dkThrow.setTotalDuration(1200);
        peachLadder.setTotalDuration(600);
        peachHammerRight.setTotalDuration(800);
        peachHammerLeft.setTotalDuration(800);
        deisy.setTotalDuration(3000);
        peachRight.setTotalDuration(600);
        peachLeft.setTotalDuration(600);
        Keyboard keyboard = janela.getKeyboard();
        
        //teste 

        while(true){
            background.draw();
            dkParado.draw();
            deisy.draw();
            peachRight.draw();
            peachLeft.draw();
            peachHammerRight.draw();
            peachHammerLeft.draw();
            peachLadder.draw();
            barrel.draw();
            dkThrow.draw();

            if (keyboard.keyDown(Keyboard.ESCAPE_KEY)) break;

            dkThrow.update();
            barrel.update();
            peachLadder.update();
            peachLeft.update();
            peachHammerLeft.update();
            peachRight.update();
            dkParado.update();
            deisy.update();
            peachHammerRight.update();
            janela.update();
        }
        janela.exit();
    }
           
}
